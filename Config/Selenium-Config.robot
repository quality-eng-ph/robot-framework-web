*** Variables ***
${BROWSER}      chrome
${HEALTH_CHECK_WINDOW_WIDTH}    480
${HEALTH_CHECK_WINDOW_HEIGHT}   320
${PAGE_LOAD_RETRY_INTERVAL}     200 milliseconds
${PAGE_LOAD_TIMEOUT}    30 seconds
${SUT_URL}      http://automationpractice.com
