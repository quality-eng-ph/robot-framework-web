==============================================================================
Tests                                                                         
==============================================================================
Tests.0-Search-Check-Links                                                    
==============================================================================
User should not encounter broken link(s) in search page               
Checking hyperlink: http://automationpractice.com/index.php?controller=search&orderby=position&orderway=desc&search_query=Printed+Dress&submit_search=
Checking hyperlink: http://automationpractice.com/index.php?controller=my-account
Checking hyperlink: http://automationpractice.com/index.php?controller=contact
Checking hyperlink: http://automationpractice.com/
Checking hyperlink: http://automationpractice.com/index.php?controller=order
Checking hyperlink: http://automationpractice.com/index.php?id_category=3&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=4&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=5&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=7&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=8&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=9&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=10&controller=category
Checking hyperlink: http://automationpractice.com/index.php?id_category=11&controller=category
Checking hyperlink: http://automationpractice.com/index.php?controller=best-sales
Checking hyperlink: http://automationpractice.com/index.php?id_product=1&controller=product
Checking hyperlink: http://automationpractice.com/index.php?id_product=7&controller=product
Checking hyperlink: http://automationpractice.com/index.php?id_product=2&controller=product
Checking hyperlink: http://automationpractice.com/index.php?id_product=5&controller=product
Checking hyperlink: http://automationpractice.com/index.php?id_product=3&controller=product
Checking hyperlink: http://automationpractice.com/index.php?id_product=6&controller=product
Checking hyperlink: http://automationpractice.com/index.php?id_cms_category=1&controller=cms
Checking hyperlink: http://automationpractice.com/index.php?id_cms=1&controller=cms
Checking hyperlink: http://automationpractice.com/index.php?id_cms=2&controller=cms
Checking hyperlink: http://automationpractice.com/index.php?id_cms=3&controller=cms
Checking hyperlink: http://automationpractice.com/index.php?id_cms=4&controller=cms
Checking hyperlink: http://automationpractice.com/index.php?id_cms=5&controller=cms
Checking hyperlink: http://automationpractice.com/index.php?controller=stores
Checking hyperlink: http://automationpractice.com/index.php?controller=manufacturer
Checking hyperlink: http://automationpractice.com/index.php?id_manufacturer=1&controller=manufacturer
Checking hyperlink: http://automationpractice.com/index.php?controller=prices-drop
Checking hyperlink: http://automationpractice.com/index.php?controller=supplier
Checking hyperlink: http://automationpractice.com/index.php?id_supplier=1&controller=supplier
Checking hyperlink: http://automationpractice.com/index.php?controller=search&orderby=position&orderway=desc&search_query=Printed+Dress&submit_search=#
Checking hyperlink: http://automationpractice.com/index.php?id_product=5&controller=product&search_query=Printed+Dress&results=5
Checking hyperlink: http://automationpractice.com/index.php?controller=cart&add=1&id_product=5&token=e817bb0705dd58da8db074c69f729fd8
Checking hyperlink: http://automationpractice.com/index.php?id_product=5&controller=product#/size-s/color-black
Checking hyperlink: http://automationpractice.com/index.php?id_product=5&controller=product#/size-s/color-orange
Checking hyperlink: http://automationpractice.com/index.php?id_product=5&controller=product#/size-s/color-blue
Checking hyperlink: http://automationpractice.com/index.php?id_product=5&controller=product#/size-s/color-yellow
Checking hyperlink: http://automationpractice.com/index.php?id_product=4&controller=product&search_query=Printed+Dress&results=5
Checking hyperlink: http://automationpractice.com/index.php?controller=cart&add=1&id_product=4&token=e817bb0705dd58da8db074c69f729fd8
Checking hyperlink: http://automationpractice.com/index.php?id_product=4&controller=product#/size-s/color-beige
Checking hyperlink: http://automationpractice.com/index.php?id_product=4&controller=product#/size-s/color-pink
Checking hyperlink: http://automationpractice.com/index.php?id_product=7&controller=product&search_query=Printed+Dress&results=5
Checking hyperlink: http://automationpractice.com/index.php?controller=cart&add=1&id_product=7&token=e817bb0705dd58da8db074c69f729fd8
Checking hyperlink: http://automationpractice.com/index.php?id_product=7&controller=product#/size-s/color-green
Checking hyperlink: http://automationpractice.com/index.php?id_product=7&controller=product#/size-s/color-yellow
Checking hyperlink: http://automationpractice.com/index.php?id_product=6&controller=product&search_query=Printed+Dress&results=5
Checking hyperlink: http://automationpractice.com/index.php?controller=cart&add=1&id_product=6&token=e817bb0705dd58da8db074c69f729fd8
Checking hyperlink: http://automationpractice.com/index.php?id_product=6&controller=product#/size-s/color-white
Checking hyperlink: http://automationpractice.com/index.php?id_product=6&controller=product#/size-s/color-yellow
Checking hyperlink: http://automationpractice.com/index.php?id_product=3&controller=product&search_query=Printed+Dress&results=5
Checking hyperlink: http://automationpractice.com/index.php?controller=cart&add=1&id_product=3&token=e817bb0705dd58da8db074c69f729fd8
Checking hyperlink: http://automationpractice.com/index.php?id_product=3&controller=product#/size-s/color-orange
Checking hyperlink: https://www.facebook.com/groups/525066904174158/
Checking hyperlink: https://twitter.com/seleniumfrmwrk
Checking hyperlink: https://www.youtube.com/channel/UCHl59sI3SRjQ-qPcTrgt0tA
Checking hyperlink: https://plus.google.com/111979135243110831526/posts
Checking hyperlink: http://automationpractice.com/index.php?controller=new-products
Checking hyperlink: http://automationpractice.com/index.php?controller=sitemap
Checking hyperlink: http://www.prestashop.com/
Checking hyperlink: http://automationpractice.com/index.php?controller=history
Checking hyperlink: http://automationpractice.com/index.php?controller=order-slip
Checking hyperlink: http://automationpractice.com/index.php?controller=addresses
Checking hyperlink: http://automationpractice.com/index.php?controller=identity
| PASS |
------------------------------------------------------------------------------
Tests.0-Search-Check-Links                                            | PASS |
1 critical test, 1 passed, 0 failed
1 test total, 1 passed, 0 failed
==============================================================================
Tests                                                                 | PASS |
1 critical test, 1 passed, 0 failed
1 test total, 1 passed, 0 failed
==============================================================================
Output:  /Users/nykerking/PycharmProjects/automation-practice/Output/pabot_results/Tests.0-Search-Check-Links/output.xml
