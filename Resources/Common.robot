*** Settings ***
Library     Collections
Library     RequestsLibrary
Library     Selenium2Library
Library     String
Resource    ${EXECDIR}${/}Config${/}Automation-Practice.robot
Resource    ${EXECDIR}${/}Config${/}Requests-Config.robot
Resource    ${EXECDIR}${/}Config${/}Selenium-Config.robot
Resource    ${EXECDIR}${/}Resources${/}Dictionary-Location.robot
Resource    ${EXECDIR}${/}Resources${/}Keywords-Universal-Footer.robot
Resource    ${EXECDIR}${/}Resources${/}Keywords-Universal-Header.robot
#Variables   ${EXECDIR}${/}Input${/}Accounts.py
Variables   ${EXECDIR}${/}Input${/}Search-Data.py
Variables   ${EXECDIR}${/}Resources${/}ResourceMapping.py

*** Variables ***
${HREF_HARVESTER}   ${EXECDIR}${/}Resources${/}href_harvester.js
${IMAGE_HARVESTER}  ${EXECDIR}${/}Resources${/}image_harvester.js

*** Keywords ***
Begin Test
    [Documentation]  Conducts a health check on the System Under Test base URL. Upon success, opens a browser then goes to the said URL.
    Common.Critical Health Check    ${SUT_URL}
    Open Browser    about:blank     ${BROWSER}
    Common.Initialize Global Variables

Cart Confirmation Modal Should Be Visible
    [Documentation]     Fails if in the current page, the cart confirmation modal is not visible.
    ${selector}=    Get Locator Value From Strategy-Locator Value Dictionary    ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['MODAL_CART_CONFIRMATION']['CONTAINER']}
    ${is_modal_displayed}=  Execute Javascript  return window.getComputedStyle(document.getElementById('${selector}'),null).getPropertyValue('display');
    Should Be Equal As Strings  ${is_modal_displayed}   block

Check For Broken Hyperlinks
    [Documentation]     Fails if a page contains a broken link.
    ${all_hrefs}=       Execute Javascript  ${HREF_HARVESTER}
    Log To Console      ${EMPTY}
    :FOR     ${element}      IN      @{all_hrefs}
    \   Log To Console      Checking hyperlink: ${element}
    \   Common.Non-critical Health Check   ${element}

Check For Broken Image Source
    [Documentation]     Fails if a page contains a broken image.
    ${all_images}=    Execute Javascript  ${IMAGE_HARVESTER}
    Log To Console      ${EMPTY}
    :FOR     ${element}      IN      @{all_images}
    \   Log To Console      Checking image src: ${element}
    \   Common.Non-critical Health Check   ${element}

Critical Health Check
    [Documentation]     Conducts a health check on the given URL via ``HTTP GET`` request method.
    ...
    ...                 Raises a _Fatal Error_ on failure.
    [Arguments]         ${URL}
    ${is_healthy}=      Common.Non-critical Health Check   ${URL}
    Run Keyword Unless      ${is_healthy}   Fatal Error     msg=Cannot connect to given URL

Does Require Certificate
    [Documentation]     Returns ``True`` if the given protocol is equal to ``https``; otherwise, returns ``False``;
    [Arguments]     ${protocol}
    Should Be Equal As Strings      ${protocol}     https

Element Should Be Visible From Current View
    [Documentation]     Fails if the given element does not return a positive floating point number.
    [Arguments]         ${locator}
    ${x_coordinate}=    Common.Get Element X-Coordinate     ${locator}
    ${x_comparison}=    Evaluate    ${x_coordinate} >= 0
    ${y_coordinate}=    Common.Get Element Y-Coordinate     ${locator}
    ${y_comparison}=    Evaluate    ${y_coordinate} >= 0
    ${logical_result}=  Evaluate    ${x_comparison} and ${y_comparison}
    Should Be True      ${logical_result}   msg=Current view does not contain the element. Current element position (${x_coordinate}, ${y_coordinate}).

Element Text Content Should Be
    [Documentation]     Fails if the text content of an element is not the same as the given value.
    [Arguments]         ${locator}  ${should_be_this_value}
    ${element_text_content}=        Common.Get Element Text Content     ${locator}
    Should Be Equal As Strings      ${element_text_content}             ${should_be_this_value}

End Test
    [Documentation]     Closes the browser and destorys all created request sessions.
    Close Browser
    Delete All Sessions

Footer Page Section Test
    [Teardown]          Common.Scroll Page To Top
    Common.Scroll Page To Bottom
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the Newsletter header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the Newsletter textbox
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Follow us header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Facebook icon
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Twitter icon
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Youtube icon
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Google Plus icon
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Categories header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Women link from Categories header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Specials link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the Specials link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see New products link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the New products link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Best sellers link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the Best sellers link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Our stores link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the Our stores link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Contact us link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the Contact us link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Terms and condtions of use link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the Terms and conditions of use link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see About us link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the About us link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Sitemap link from Information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the Sitemap link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see My account header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see My orders link from My account header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the My orders link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see My credit slips link from My account header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the My credit slips link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see My addresses link from My account header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the My addresses link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see My personal info link from My account header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the correct tooltip text of the My personal info link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see Store information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the address from Store information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the telephone number from Store information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the email address from Store information header
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to email the store by clicking the email address link
    Run Keyword And Continue On Failure     Keywords-Universal-Footer.User should be able to see the copyright

Get Element Background Color
    [Documentation]     Returns the background color of an element
    [Arguments]         ${locator}
    ${selector}=        Common.Get Locator Value From Strategy-Locator Value Dictionary     ${locator}
    ${background_color}=    Execute Javascript  return window.getComputedStyle(document.querySelector('${selector}'),null).getPropertyValue('background-color');
    [Return]    ${background_color}

Get Element Text Color
    [Documentation]     Returns the color of an element
    [Arguments]         ${locator}
    ${selector}=        Common.Get Locator Value From Strategy-Locator Value Dictionary     ${locator}
    ${text_color}=      Execute Javascript  return window.getComputedStyle(document.querySelector('${selector}'),null).getPropertyValue('color');
    [Return]    ${text_color}

Get Element Text Content
    [Documentation]     Returns the text content of an element.
    [Arguments]         ${locator}
    ${selector}=        Common.Get Locator Value From Strategy-Locator Value Dictionary     ${locator}
    ${element_text_content}=    Execute Javascript  return document.querySelector('${selector}').textContent
    [Return]    ${element_text_content}

Get Img Src
    [Documentation]     Returns the source _URL_ of an element.
    [Arguments]     ${locator}
    ${img_url}=     Get Element Attribute   ${locator}@src
    [Return]        ${img_url}

Get Locator Value From Strategy-Locator Value Dictionary
    [Documentation]     Returns the Locator Value from the Strategy-Locator Value.
    [Arguments]         ${strategy_locator_value_dictionary}
    ${prefix}   ${locator_value}=   Split String    ${strategy_locator_value_dictionary}    =       max_split=1
    [Return]    ${locator_value}

Get Page Favicon URL
    [Documentation]     Returns a string that specifies the favicon URL of a page.
    ${favicon_URL}=     Get Element Attribute   ${UNIVERSAL_OBJECTS['FAVICON']}@href
    [Return]        ${favicon_URL}

Get Protocol
    [Documentation]     Returns the first five characters of the given _URL_.
    [Arguments]     ${URL}
    ${protocol}=    Get Substring   string=${URL}      start=0       end=5
    [Return]        ${protocol}

Has Trailing Forward Slash
    [Documentation]     Returns ``True`` if the given _URL_ has a forward slash character ``/`` in the end; otherwise, returns ``False``.
    [Arguments]     ${URL}
    ${last_char}=   Get Substring   ${URL}  -1
    ${has_slash}=   Run Keyword And Return Status     Should Be Equal As Strings  ${last_char}    /
    [Return]        ${has_slash}

Header Page Section Test
    Common.Scroll Page To Top
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the Contact us button
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Contact us button
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the Sign in button
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Sign in button
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the logo
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct alternate text of the logo
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the search bar
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the search button
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the cart menu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should not be able to see the initial quantity of the cart menu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should not be able to see the initial singular quantifier of the cart menu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should not be able to see the initial plural quantifier of the cart menu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should not be able to see the initial total price of the cart menu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the initial content of the cart menu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the Women tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Women tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Women submenu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should see highlighted Women tab upon hover
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Tops submenu header from Women tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Tops submenu header from the Women tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see T-shirts and Blouses submenus from Tops submenu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the T-shirts and Blouses submenus from the Tops submenu header
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Dresses submenu header from Women tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Dresses submenu header from the Women tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Casual Dresses, Evening Dresses, and Summer Dresses submenus from Dresses submenu header
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Casual Dresses, Evening Dresses, and Summer Dresses submenus from the Dresses submenu header
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Dresses submenu
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should see highlighted Dresses tab upon hover
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Casual Dresses submenu from Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Casual Dresses submenu from the Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Evening Dresses submenu from Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Evening Dresses submenu from the Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see Summer Dresses submenu from Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the Summer Dresses submenu from the Dresses tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the T-shirts tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should be able to see the correct tooltip text of the T-shirts tab
    Run Keyword And Continue On Failure     Keywords-Universal-Header.User should see highlighted T-shirts tab upon hover

Initialize Global Variables
    [Documentation]     Initializes or prepares the global variables to be used.
    # Search keyword:
    ${COMMON_search_keyword}=   Common.Pick Randomly From Arguments      @{SEARCH_QUERY}
    Set Global Variable     ${COMMON_search_keyword}    ${COMMON_search_keyword}
    # Category:
    ${women_tops_category}=     Get Dictionary Values   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']}
    ${women_dresses_category}=  Get Dictionary Values   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']}
    ${dresses_category}=    Get Dictionary Values   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']}
    ${COMMON_random_category}=  Common.Pick Randomly From Arguments
    ...                             ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}
    ...                             ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    ...                             ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_TAB_T-SHIRTS']}
    ...                             ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['LINK_WOMEN']}
    ...                             ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SPECIALS']}
    ...                             ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_BEST_SELLERS']}
    Set Global Variable     ${COMMON_random_category}   ${COMMON_random_category}

Non-critical Health Check
    [Documentation]     Returns ``True`` if the given _URL_ responded with a status code ``200``; otherwise, returns ``False``.
    ...
    ...                 Conducts a health check on the given URL via ``HTTP GET`` request method.
    ...
    ...                 Does *not* raise a _Fatal Error_ on failure.
    [Arguments]     ${URL}
    [Teardown]      Delete All Sessions
    ${protocol}=			Common.Get Protocol     ${URL}
	${session_verify}=		Run Keyword And Return Status           Common.Does Require Certificate     ${protocol}
    Create Session          health_check            ${URL}          verify=${session_verify}
    ${resp_health_check}=   Run Keyword And Continue On Failure     Get Request             health_check    /   allow_redirects=True
    Run Keyword And Ignore Error    Log     ${resp_health_check.history}
    Run Keyword And Ignore Error    Log     ${resp_health_check.url}
    Run Keyword And Ignore Error    Log     ${resp_health_check.headers}
    Run Keyword And Ignore Error    Log     ${resp_health_check.text}
    ${is_healthy}=          Run Keyword And Return Status           Should Be Equal As Strings          ${resp_health_check.status_code}     200
    [Return]                ${is_healthy}

Page Should Be Loaded
    [Documentation]     Fails if the page has *not* yet completed loading.
    ${is_page_loaded}=  Execute Javascript  return document.readyState === "complete"
    Should Be Equal As Strings  ${is_page_loaded}   True

Pick Randomly From Arguments
    [Documentation]     Returns a random object from the given list.
    [Arguments]         @{list}
    ${list_length}=     Get Length  ${list}
    ${random_index}=    Evaluate    random.randint(0, ${list_length}-1)    modules=random
    [Return]            ${list[${random_index}]}

Pick Randomly From Integer Range
    [Documentation]     Returns a random integer from the given range.
    ...
    ...                 Range covers both min and max. [_min_, _max_]
    [Arguments]         ${min}      ${max}
    ${integer}=         Evaluate    random.randint(${min}, ${max})    modules=random
    [Return]            ${integer}

Scroll Page To Bottom
    [Documentation]     Scrolls the page to bottom-most section of the screen.
    Common.Scroll Page To Location  0   Number.MAX_SAFE_INTEGER     #javascript number attribute; Number.MAX_SAFE_INTEGER = 9007199254740991

Scroll Page To Location
    [Documentation]     Scrolls the page to _x_ and _y_.
    [Arguments]    ${x}    ${y}
    Execute JavaScript    window.scrollTo(${x},${y})

Scroll Page To Top
    [Documentation]     Scrolls the page to top-most section of the screen.
    Common.Scroll Page To Location  0   Number.MIN_SAFE_INTEGER     #javascript number attribute; Number.MIN_SAFE_INTEGER = -9007199254740991

Wait Until Cart Confirmation Modal Is Visible
    [Documentation]     Waits until cart confirmation modal is visible or until _retry_ timeout is reached; whichever goes first.
    Wait Until Keyword Succeeds     ${PAGE_LOAD_TIMEOUT}  ${PAGE_LOAD_RETRY_INTERVAL}  Common.Cart Confirmation Modal Should Be Visible

Wait Until Page Finishes Loading
    [Documentation]     Waits until page has finished loading or until _retry_ timeout is reached; whichever goes first.
    Wait Until Keyword Succeeds     ${PAGE_LOAD_TIMEOUT}  ${PAGE_LOAD_RETRY_INTERVAL}  Common.Page Should Be Loaded