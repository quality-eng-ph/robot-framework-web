*** Settings ***
Resource        ${EXECDIR}${/}Resources${/}Common.robot

*** Variables ***
${LOCATION_PRETASHOP_REFERRAL}  https://www.prestashop.com/?utm_source=v16_homeslider

${LOCATION_HOME_PAGE}   ${SUT_URL}/index.php
${LOCATION_CONTACT_US}  ${SUT_URL}/index.php?controller=contact
${LOCATION_SIGN_IN}     ${SUT_URL}/index.php?controller=authentication
${LOCATION_SEARCH}      ${SUT_URL}/index.php?controller=search
${LOCATION_CHECKOUT}    ${SUT_URL}/index.php?controller=order