*** Settings ***
Resource        ${EXECDIR}${/}Resources${/}Common.robot

*** Keywords ***
User should be able to see the Newsletter header
    [Tags]      footer_section  smoke   visuals     newsletter
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['NEWSLETTER']['HEADER']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['NEWSLETTER']['HEADER']}          Newsletter

User should be able to see the Newsletter textbox
    [Tags]      footer_section  smoke   visuals     newsletter
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['NEWSLETTER']['TEXTBOX']}
    ${placeholder_value}=       Get Element Attribute   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['NEWSLETTER']['TEXTBOX']}@value
    Should Be Equal As Strings  ${placeholder_value}    Enter your e-mail
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['NEWSLETTER']['BUTTON_SUBMIT']}

User should be able to see Follow us header
    [Tags]      footer_section  smoke   visuals     social
    Page Should Contain Element     ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['SOCIAL']['HEADER']}
    Element Text Should Be          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['SOCIAL']['HEADER']}      Follow us

User should be able to see Facebook icon
    [Tags]      footer_section  smoke   visuals     social
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['SOCIAL']['LINK_FACEBOOK']}

User should be able to see Twitter icon
    [Tags]      footer_section  smoke   visuals     social
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['SOCIAL']['LINK_TWITTER']}

User should be able to see Youtube icon
    [Tags]      footer_section  smoke   visuals     social
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['SOCIAL']['LINK_YOUTUBE']}

User should be able to see Google Plus icon
    [Tags]      footer_section  smoke   visuals     social
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['SOCIAL']['LINK_GOOGLE_PLUS']}

User should be able to see Categories header
    [Tags]      footer_section  smoke   visuals     categories
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['HEADER']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['HEADER']}          Categories

User should be able to see Women link from Categories header
    [Tags]      footer_section  smoke   visuals     categories
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['LINK_WOMEN']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['LINK_WOMEN']}      Women
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['LINK_WOMEN']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['CATEGORIES']['LINK_WOMEN']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['HEADER']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['HEADER']}      Information

User should be able to see Specials link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SPECIALS']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SPECIALS']}  Specials
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SPECIALS']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SPECIALS']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the Specials link
    [Documentation]     Asserts that the tooltip text of the Specials link is _Specials_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SPECIALS']}@title
    Should Be Equal As Strings  ${tooltip_text}     Specials

User should be able to see New products link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_NEW_PRODUCTS']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_NEW_PRODUCTS']}  New products
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_NEW_PRODUCTS']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_NEW_PRODUCTS']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the New products link
    [Documentation]     Asserts that the tooltip text of the New products link is _New products_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_NEW_PRODUCTS']}@title
    Should Be Equal As Strings  ${tooltip_text}     New products

User should be able to see Best sellers link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_BEST_SELLERS']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_BEST_SELLERS']}  Best sellers
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_BEST_SELLERS']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_BEST_SELLERS']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the Best sellers link
    [Documentation]     Asserts that the tooltip text of the Best sellers link is _Best sellers_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_BEST_SELLERS']}@title
    Should Be Equal As Strings  ${tooltip_text}     Best sellers

User should be able to see Our stores link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_OUR_STORES']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_OUR_STORES']}  Our stores
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_OUR_STORES']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_OUR_STORES']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the Our stores link
    [Documentation]     Asserts that the tooltip text of the Our stores is _Our stores_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_OUR_STORES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Our stores

User should be able to see Contact us link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_CONTACT_US']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_CONTACT_US']}  Contact us
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_CONTACT_US']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_CONTACT_US']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the Contact us link
    [Documentation]     Asserts that the tooltip text of the Contact us is _Contact us_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_CONTACT_US']}@title
    Should Be Equal As Strings  ${tooltip_text}     Contact us

User should be able to see Terms and condtions of use link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_TERMS_AND_CONDITIONS_OF_USE']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_TERMS_AND_CONDITIONS_OF_USE']}  Terms and conditions of use
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_TERMS_AND_CONDITIONS_OF_USE']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_TERMS_AND_CONDITIONS_OF_USE']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the Terms and conditions of use link
    [Documentation]     Asserts that the tooltip text of the Terms and conditions of use link is _Terms and conditions of use link_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_TERMS_AND_CONDITIONS_OF_USE']}@title
    Should Be Equal As Strings  ${tooltip_text}     Terms and conditions of use

User should be able to see About us link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_ABOUT_US']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_ABOUT_US']}  About us
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_ABOUT_US']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_ABOUT_US']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the About us link
    [Documentation]     Asserts that the tooltip text of the About us is _About us_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_ABOUT_US']}@title
    Should Be Equal As Strings  ${tooltip_text}     About us

User should be able to see Sitemap link from Information header
    [Tags]      footer_section  smoke   visuals     information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SITEMAP']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SITEMAP']}  Sitemap
    Mouse Over          ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SITEMAP']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SITEMAP']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the Sitemap link
    [Documentation]     Asserts that the tooltip text of the Sitemap is _Sitemap_.
    [Tags]      footer_section  aesthetics  visuals     information
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['INFORMATION']['LINK_SITEMAP']}@title
    Should Be Equal As Strings  ${tooltip_text}     Sitemap

User should be able to see My account header
    [Tags]      footer_section  smoke   visuals     my_account
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['HEADER']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['HEADER']}      My account

User should be able to see My orders link from My account header
    [Tags]      footer_section  smoke   visuals     my_account
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ORDERS']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ORDERS']}  My orders
    Mouse Over                  ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ORDERS']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ORDERS']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the My orders link
    [Documentation]     Asserts that the tooltip text of the My orders is _My orders_.
    [Tags]      footer_section  aesthetics  visuals     my_account
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ORDERS']}@title
    Should Be Equal As Strings  ${tooltip_text}     My orders

User should be able to see My credit slips link from My account header
    [Tags]      footer_section  smoke   visuals     my_account
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_CREDIT_SLIPS']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_CREDIT_SLIPS']}  My credit slips
    Mouse Over                  ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_CREDIT_SLIPS']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_CREDIT_SLIPS']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the My credit slips link
    [Documentation]     Asserts that the tooltip text of the My credit slips is _My credit slips_.
    [Tags]      footer_section  aesthetics  visuals     my_account
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_CREDIT_SLIPS']}@title
    Should Be Equal As Strings  ${tooltip_text}     My credit slips

User should be able to see My addresses link from My account header
    [Tags]      footer_section  smoke   visuals     my_account
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ADDRESSES']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ADDRESSES']}  My addresses
    Mouse Over                  ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ADDRESSES']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ADDRESSES']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the My addresses link
    [Documentation]     Asserts that the tooltip text of the My addresses is _My addresses_.
    [Tags]      footer_section  aesthetics  visuals     my_account
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_ADDRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     My addresses

User should be able to see My personal info link from My account header
    [Tags]      footer_section  smoke   visuals     my_account
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_PERSONAL_INFO']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_PERSONAL_INFO']}  My personal info
    Mouse Over                  ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_PERSONAL_INFO']}
    ${text_color}=      Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_PERSONAL_INFO']}
    Should Be Equal As Strings  ${text_color}   rgb(255, 255, 255)

User should be able to see the correct tooltip text of the My personal info link
    [Documentation]     Asserts that the tooltip text of the My personal info is _Manage my personal information_.
    [Tags]      footer_section  aesthetics  visuals     my_account
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['MY_ACCOUNT']['MY_PERSONAL_INFO']}@title
    Should Be Equal As Strings  ${tooltip_text}     Manage my personal information

User should be able to see Store information header
    [Tags]      footer_section  smoke   visuals     store_information
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['HEADER']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['HEADER']}      Store information

User should be able to see the address from Store information header
    [Tags]      footer_section  smoke  visuals     store_infomration
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['ADDRESSES']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['ADDRESSES']}    Selenium Framework, Research Triangle Park, North Carolina, USA

User should be able to see the telephone number from Store information header
    [Tags]      footer_section  aesthetics  visuals     store_infomration
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['TELEPHONE_NUMBER']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['TELEPHONE_NUMBER']}    Call us now: (347) 466-7432

User should be able to see the email address from Store information header
    [Tags]      footer_section  aesthetics  visuals     store_infomration
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['EMAIL_ADDRESS']['LABEL']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['EMAIL_ADDRESS']['LABEL']}    Email: support@seleniumframework.com

User should be able to email the store by clicking the email address link
    [Tags]      footer_section  smoke  visuals      store_infomration
    ${mail_to_link}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['STORE_INFORMATION']['EMAIL_ADDRESS']['VALUE']}@href
    Should Be Equal As Strings  ${mail_to_link}     mailto:%73%75%70%70%6f%72%74@%73%65%6c%65%6e%69%75%6d%66%72%61%6d%65%77%6f%72%6b.%63%6f%6d

User should be able to see the copyright
    [Tags]      footer_section  smoke   visuals     copyright
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['COPYRIGHT']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['FOOTER_SECTION']['COPYRIGHT']}     © 2014 Ecommerce software by PrestaShop™