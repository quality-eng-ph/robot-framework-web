*** Settings ***
Resource        ${EXECDIR}${/}Resources${/}Common.robot

*** Keywords ***
Click Contact Us Button
    [Tags]      header_section
    Common.Scroll Page To Top
    Click Element   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_CONTACT_US']}

Click Header Logo
    [Tags]      header_section
    Common.Scroll Page To Top
    Click Element   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_LOGO']}

Click Sign In Button
    [Tags]      header_section
    Common.Scroll Page To Top
    Click Element   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_SIGN_IN']}

#   ---     Test Cases in Keyword Form for Easy Calls   ---     #

User should be able to see the Contact us button
    [Tags]      header_section  smoke   visuals     contact_us
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_CONTACT_US']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_CONTACT_US']}   Contact us

User should be able to see the correct tooltip text of the Contact us button
    [Documentation]     Asserts that the tooltip text of the Contact us button is _Contact Us_.
    [Tags]      header_section  aesthetics  visuals     contact_us
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_CONTACT_US']}@title
    Should Be Equal As Strings  ${tooltip_text}     Contact Us

User should be able to see the Sign in button
    [Tags]      header_section  smoke   visuals     sign_in
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_SIGN_IN']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_SIGN_IN']}      Sign in

User should be able to see the correct tooltip text of the Sign in button
    [Documentation]     Asserts that the tooltip text of the Contact us button is _Log in to your customer account_.
    [Tags]      header_section  aesthetics  visuals     sign_in
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_SIGN_IN']}@title
    Should Be Equal As Strings  ${tooltip_text}     Log in to your customer account

User should be able to see the logo
    [Documentation]     Asserts that the logo is visible
    [Tags]      header_section  smoke   visuals     logo
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_LOGO']}

User should be able to see the correct alternate text of the logo
    [Documentation]     Asserts that the alternate text of the logo is _My Store_.
    [Tags]      header_section  aesthetics  visuals     logo
    ${alt_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_LOGO']}@alt
    Should Be Equal As Strings  ${alt_text}     My Store

User should be able to see the search bar
    [Tags]      header_section  smoke   visuals     search_bar
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}
    ${placeholder_text}=        Get Element Attribute   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}@placeholder
    Should Be Equal As Strings  ${placeholder_text}     Search

User should be able to see the search button
    [Tags]      header_section  smoke   visuals     search_bar
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['BUTTON_SEARCH']}

User should be able to see the cart menu
    [Tags]      header_section  smoke   visuals     cart
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['CART_MENU']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_TEXT']}     Cart

User should not be able to see the initial quantity of the cart menu
    [Tags]      header_section  visuals     initial     cart
    Element Should Not Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_QUANTITY']}
    Common.Element Text Content Should Be   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_QUANTITY']}      0

User should not be able to see the initial singular quantifier of the cart menu
    [Tags]      header_section  visuals     initial     cart
    Element Should Not Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_SINGULAR_QUANTIFIER']}
    Common.Element Text Content Should Be   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_SINGULAR_QUANTIFIER']}   Product

User should not be able to see the initial plural quantifier of the cart menu
    [Tags]      header_section  visuals     initial     cart
    Element Should Not Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_PLURAL_QUANTIFIER']}
    Common.Element Text Content Should Be   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_PLURAL_QUANTIFIER']}     Products

User should not be able to see the initial total price of the cart menu
    [Tags]      header_section  visuals     initial     cart
    Element Should Not Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_TOTAL_PRICE']}
    ${cart_menu_total_price}=       Common.Get Element Text Content     ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_TOTAL_PRICE']}
    ${cart_menu_total_price}=       Strip String    ${cart_menu_total_price}
    Should Be Equal As Strings      ${cart_menu_total_price}    ${EMPTY}

User should be able to see the initial content of the cart menu
    [Tags]      header_section  visuals     initial     cart
    Element Should Be Visible       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_EMPTINESS']}
    Element Text Should Be          ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DROPDOWN_CART_MENU']['ATTR_EMPTINESS']}    (empty)

User should be able to see the Women tab
    [Tags]      header_section  smoke   visuals     women_tab
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}      WOMEN

User should be able to see the correct tooltip text of the Women tab
    [Documentation]     Asserts that the tooltip text of the Women is _Women_.
    [Tags]      header_section  aesthetics  visuals     women_tab
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}@title
    Should Be Equal As Strings  ${tooltip_text}     Women

User should be able to see Women submenu
    [Tags]      header_section  smoke   visuals     women_tab
    Mouse Over  ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}

User should see highlighted Women tab upon hover
    [Tags]      header_section  aesthetics  visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    ${background_color}=    Common.Get Element Background Color     ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}
    Should Be Equal As Strings  ${background_color}     rgb(51, 51, 51)
    ${text_color}=          Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['LINK_TAB_WOMEN']}
    Should Be Equal As Strings  ${text_color}     rgb(255, 255, 255)

User should be able to see Tops submenu header from Women tab
    [Tags]      header_section  smoke   visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']['HEADER_TOPS']}

User should be able to see the correct tooltip text of the Tops submenu header from the Women tab
    [Documentation]     Asserts that the tooltip text of the Women is _Tops_.
    [Tags]      header_section  aesthetics  visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']['HEADER_TOPS']}@title
    Should Be Equal As Strings  ${tooltip_text}     Tops

User should be able to see T-shirts and Blouses submenus from Tops submenu
    [Tags]      header_section  smoke   visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']['LINK_T-SHIRTS']}
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']['LINK_BLOUSES']}

User should be able to see the correct tooltip text of the T-shirts and Blouses submenus from the Tops submenu header
    [Documentation]     Asserts that the tooltip text of the T-shirts and Blouses are _T-shirts_ and _Blouses_ respectively.
    [Tags]      header_section  aesthetics  visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']['LINK_T-SHIRTS']}@title
    Should Be Equal As Strings  ${tooltip_text}     T-shirts
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['TOPS_SUBMENU']['LINK_BLOUSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Blouses

User should be able to see Dresses submenu header from Women tab
    [Tags]      header_section  smoke   visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['HEADER_DRESSES']}

User should be able to see the correct tooltip text of the Dresses submenu header from the Women tab
    [Documentation]     Asserts that the tooltip text of the Women is _Dresses_.
    [Tags]      header_section  aesthetics  visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['HEADER_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Dresses

User should be able to see Casual Dresses, Evening Dresses, and Summer Dresses submenus from Dresses submenu header
    [Tags]      header_section  smoke   visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['LINK_CASUAL_DRESSES']}
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['LINK_EVENING_DRESSES']}
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['LINK_SUMMER_DRESSES']}

User should be able to see the correct tooltip text of the Casual Dresses, Evening Dresses, and Summer Dresses submenus from the Dresses submenu header
    [Documentation]     Asserts that the tooltip text of the Casual Dresses, Evening Dresses, and Summer Dresses are _Casual Dresses_, _Evening Dresses_, and _Evening Dresses_ respectively.
    [Tags]      header_section  aesthetics  visuals     women_tab
    Keywords-Universal-Header.User should be able to see Women submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['LINK_CASUAL_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Casual Dresses
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['LINK_EVENING_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Evening Dresses
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['WOMEN_TAB']['WOMEN_TAB_SUBMENU']['DRESSES_SUBMENU']['LINK_SUMMER_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Summer Dresses

User should be able to see the Dresses tab
    [Tags]      header_section  smoke   visuals     dresses_tab
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}    DRESSES

User should be able to see the correct tooltip text of the Dresses tab
    [Documentation]     Asserts that the tooltip text of the Dresses is _Dresses_.
    [Tags]      header_section  aesthetics  visuals     dresses_tab
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Dresses

User should be able to see Dresses submenu
    [Tags]      header_section  smoke   visuals     dresses_tab
    Mouse Over  ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['SUBMENU_CONTAINER']}

User should see highlighted Dresses tab upon hover
    [Tags]      header_section  aesthetics  visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    ${background_color}=    Common.Get Element Background Color     ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    Should Be Equal As Strings  ${background_color}     rgb(51, 51, 51)
    ${text_color}=  Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    Should Be Equal As Strings  ${text_color}           rgb(255, 255, 255)

User should be able to see Casual Dresses submenu from Dresses tab
    [Tags]      header_section  smoke   visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['LINK_CASUAL_DRESSES']}

User should be able to see the correct tooltip text of the Casual Dresses submenu from the Dresses tab
    [Documentation]     Asserts that the tooltip text of the Casual Dresses is _Casual Dresses_.
    [Tags]      header_section  aesthetics  visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['LINK_CASUAL_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Casual Dresses

User should be able to see Evening Dresses submenu from Dresses tab
    [Tags]      header_section  smoke   visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['LINK_EVENING_DRESSES']}

User should be able to see the correct tooltip text of the Evening Dresses submenu from the Dresses tab
    [Documentation]     Asserts that the tooltip text of the Evening Dresses is _Evening Dresses_.
    [Tags]      header_section  aesthetics  visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['LINK_EVENING_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Evening Dresses

User should be able to see Summer Dresses submenu from Dresses tab
    [Tags]      header_section  smoke   visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    Wait Until Element Is Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['LINK_SUMMER_DRESSES']}

User should be able to see the correct tooltip text of the Summer Dresses submenu from the Dresses tab
    [Documentation]     Asserts that the tooltip text of the Summer Dresses is _Summer Dresses_.
    [Tags]      header_section  aesthetics  visuals     dresses_tab
    Keywords-Universal-Header.User should be able to see Dresses submenu
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['DRESSES_TAB_SUBMENU']['LINK_SUMMER_DRESSES']}@title
    Should Be Equal As Strings  ${tooltip_text}     Summer Dresses

User should be able to see the T-shirts tab
    [Tags]      header_section  smoke   visuals     t_shirts_tab
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_TAB_T-SHIRTS']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_TAB_T-SHIRTS']}   T-SHIRTS

User should be able to see the correct tooltip text of the T-shirts tab
    [Documentation]     Asserts that the tooltip text of the Women is _T-shirts_.
    [Tags]      header_section  aesthetics  visuals     t_shirts_tab
    ${tooltip_text}=    Get Element Attribute       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_TAB_T-SHIRTS']}@title
    Should Be Equal As Strings  ${tooltip_text}     T-shirts

User should see highlighted T-shirts tab upon hover
    [Tags]      header_section  aesthetics  visuals     t_shirts_tab
    Mouse Over  ${UNIVERSAL_OBJECTS['HEADER_SECTION']['LINK_TAB_T-SHIRTS']}
    ${background_color}=    Common.Get Element Background Color     ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    Should Be Equal As Strings  ${background_color}     rgb(51, 51, 51)
    ${text_color}=  Common.Get Element Text Color   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['DRESSES_TAB']['LINK_TAB_DRESSES']}
    Should Be Equal As Strings  ${text_color}           rgb(255, 255, 255)