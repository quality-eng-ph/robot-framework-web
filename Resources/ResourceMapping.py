UNIVERSAL_OBJECTS = {
    "FAVICON": "xpath=//link[@rel='icon']",
    "HEADER_SECTION": {
        "LINK_CONTACT_US": "css=#contact-link > a",
        "LINK_SIGN_IN": "css=#header > div.nav > div > div > nav > div.header_user_info > a",
        "LINK_LOGO": "css=#header_logo > a > img",
        "TEXTBOX_SEARCH_BAR": "id=search_query_top",
        "BUTTON_SEARCH": "css=#searchbox > button",
        "DROPDOWN_CART_MENU": {
            "CART_MENU": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a",
            "ATTR_TEXT": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a > b",
            "ATTR_QUANTITY": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a > span:nth-child(2)",
            "ATTR_SINGULAR_QUANTIFIER": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a > span:nth-child(3)",
            "ATTR_PLURAL_QUANTIFIER": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a > span:nth-child(4)",
            "ATTR_TOTAL_PRICE": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a > span:nth-child(5)",
            "ATTR_EMPTINESS": "css=#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a > span:nth-child(6)"
        },
        "WOMEN_TAB": {
            "LINK_TAB_WOMEN": "css=#block_top_menu > ul > li:nth-child(1) > a",
            "WOMEN_TAB_SUBMENU": {
                "SUBMENU_CONTAINER": "css=#block_top_menu > ul > li:nth-child(1) > ul",
                "TOPS_SUBMENU": {
                    "HEADER_TOPS": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(1) > a",
                    "LINK_T-SHIRTS": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(1) > ul > li:nth-child(1) > a",
                    "LINK_BLOUSES": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(1) > ul > li:nth-child(2) > a"
                },
                "DRESSES_SUBMENU": {
                    "HEADER_DRESSES": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > a",
                    "LINK_CASUAL_DRESSES": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(1) > a",
                    "LINK_EVENING_DRESSES": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(2) > a",
                    "LINK_SUMMER_DRESSES": "css=#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(3) > a"
                }
            }
        },
        "DRESSES_TAB": {
            "LINK_TAB_DRESSES": "css=#block_top_menu > ul > li:nth-child(2) > a",
            "DRESSES_TAB_SUBMENU": {
                "SUBMENU_CONTAINER": "css=#block_top_menu > ul > li:nth-child(2) > ul",
                "LINK_CASUAL_DRESSES": "css=#block_top_menu > ul > li:nth-child(2) > ul > li:nth-child(1) > a",
                "LINK_EVENING_DRESSES": "css=#block_top_menu > ul > li:nth-child(2) > ul > li:nth-child(2) > a",
                "LINK_SUMMER_DRESSES": "css=#block_top_menu > ul > li:nth-child(2) > ul > li:nth-child(3) > a"
            }
        },
        "LINK_TAB_T-SHIRTS": "css=#block_top_menu > ul > li:nth-child(3) > a"
    },
    "FOOTER_SECTION": {
        "NEWSLETTER": {
            "HEADER": "css=#newsletter_block_left > h4",
            "TEXTBOX": "id=newsletter-input",
            "BUTTON_SUBMIT": "css=#newsletter_block_left > div > form > div > button"
        },
        "SOCIAL": {
            "HEADER": "css=#social_block > h4",
            "LINK_FACEBOOK": "css=#social_block > ul > li.facebook > a",
            "LINK_TWITTER": "css=#social_block > ul > li.twitter > a",
            "LINK_YOUTUBE": "css=#social_block > ul > li.youtube > a",
            "LINK_GOOGLE_PLUS": "css=#social_block > ul > li.google-plus > a"
        },
        "CATEGORIES": {
            "HEADER": "css=#footer > div > section.blockcategories_footer.footer-block.col-xs-12.col-sm-2 > h4",
            "LINK_WOMEN": "css=#footer > div > section.blockcategories_footer.footer-block.col-xs-12.col-sm-2 > div > div > ul > li > a"
        },
        "INFORMATION": {
            "HEADER": "css=#block_various_links_footer > h4",
            "LINK_SPECIALS": "css=#block_various_links_footer > ul > li:nth-child(1) > a",
            "LINK_NEW_PRODUCTS": "css=#block_various_links_footer > ul > li:nth-child(2) > a",
            "LINK_BEST_SELLERS": "css=#block_various_links_footer > ul > li:nth-child(3) > a",
            "LINK_OUR_STORES": "css=#block_various_links_footer > ul > li:nth-child(4) > a",
            "LINK_CONTACT_US": "css=#block_various_links_footer > ul > li:nth-child(5) > a",
            "LINK_TERMS_AND_CONDITIONS_OF_USE": "css=#block_various_links_footer > ul > li:nth-child(6) > a",
            "LINK_ABOUT_US": "css=#block_various_links_footer > ul > li:nth-child(7) > a",
            "LINK_SITEMAP": "css=#block_various_links_footer > ul > li:nth-child(8) > a"
        },
        "MY_ACCOUNT": {
            "HEADER": "css=#footer > div > section:nth-child(7) > h4 > a",
            "MY_ORDERS": "css=#footer > div > section:nth-child(7) > div > ul > li:nth-child(1) > a",
            "MY_CREDIT_SLIPS": "css=#footer > div > section:nth-child(7) > div > ul > li:nth-child(2) > a",
            "MY_ADDRESSES": "css=#footer > div > section:nth-child(7) > div > ul > li:nth-child(3) > a",
            "MY_PERSONAL_INFO": "css=#footer > div > section:nth-child(7) > div > ul > li:nth-child(4) > a"
        },
        "STORE_INFORMATION": {
            "HEADER": "css=#block_contact_infos > div > h4",
            "ADDRESSES": "css=#block_contact_infos > div > ul > li:nth-child(1)",
            "TELEPHONE_NUMBER": "xpath=//*[@id='block_contact_infos']/div/ul/li[2]",
            "EMAIL_ADDRESS": {
                "LABEL": "xpath=//*[@id='block_contact_infos']/div/ul/li[3]",
                "VALUE": "css=#block_contact_infos > div > ul > li:nth-child(3) > span > a"
            }
        },
        "COPYRIGHT": "xpath=//*[@id='footer']/div/section[4]/div"
    },
    "MAIN_BODY_SECTION": {
        "BREADCRUMB": {
            "SECTION": "css=#columns > div.breadcrumb.clearfix",
            "LINK_SPECIAL_1ST_TRAIL": "css=#columns > div.breadcrumb.clearfix > span.navigation_page",
            "ONE_BASE_INDEXED_ARRAY_TRAIL": "xpath=//*[@id='columns']/div[1]/a"
            # Usage for home: ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['TRAIL']}[1]
            # Usage for 1st trail: ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['TRAIL']}[2]
            # Usage for 2nd trail: ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['TRAIL']}[3]
        },
        "TEXT_SEARCH_KEYWORD": "css=#center_column > h1 > span.lighter",
        "TEXT_NUMBER_OF_HITS_FOUND": "css=#center_column > h1 > span.heading-counter",
        "LABEL_SORT_BY": "css=#productsSortForm > div > label",
        "DROPDOWN_SORT_BY": "id=selectProductSort",
        "LINK_VIEW_GRID": "css=#grid > a",
        "LINK_VIEW_LIST": "css=#list > a",
        "HITS": {
            "EVAL_PRODUCT_LIST_COUNTER": "#center_column > ul",
            "ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST": "xpath=//*[@id='center_column']/ul/li",
            "TEXT_PRODUCT_NAME": "/div/div[2]/h5/a",
            "TEXT_PRODUCT_PRICE": "/div/div[2]/div[1]/span[1]",
            "TEXT_PRODUCT_OLD_PRICE": "/div/div[2]/div[1]/span[2]",
            "TEXT_PRODUCT_DISCOUNT": "/div/div[2]/div[1]/span[3]",
            "ONE_BASE_INDEXED_ARRAY_PRODUCT_COLOR": "/div/div[2]/div[3]/ul/li",
            "BUTTON_ADD_TO_CART": "/div/div[2]/div[2]/a[1]",
            "BUTTON_MORE": "/div/div[2]/div[2]/a[2]",
            "TEXT_PRODUCT_AVAILABILITY": "/div/div[2]/span/span"
        },
        "MODAL_CART_CONFIRMATION": {
            "CONTAINER": "id=layer_cart",
            "BUTTON_CLOSE": "css=#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > span",
            "BUTTON_CONTINUE_SHOPPING": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > span",
            "BUTTON_PROCEED_TO_CHECKOUT": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > a",
            "PRODUCT_ITEM": {
                "SPIEL_SUCCESSFUL_ADD_TO_CART": "css=#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > h2",
                "IMAGE_PRODUCT": "css=#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > div.product-image-container.layer_cart_img > img",
                "TEXT_PRODUCT_NAME": "id=layer_cart_product_title",
                "TEXT_PRODUCT_ATTRIBUTES": "id=layer_cart_product_attributes",
                "LABEL_PRODUCT_QUANTITY": "css=#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > div.layer_cart_product_info > div:nth-child(3) > strong",
                "TEXT_PRODUCT_QUANTITY": "id=layer_cart_product_quantity",
                "LABEL_PRODUCT_TOTAL": "css=#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > div.layer_cart_product_info > div:nth-child(4) > strong",
                "TEXT_PRODUCT_TOTAL": "id=layer_cart_product_price",
            },
            "CART": {
                "SPIEL_TOTAL_CART_ITEMS": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > h2 > span.ajax_cart_product_txt",
                "LABEL_TOTAL_PRODUCTS_NET_PRICE": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div:nth-child(2) > strong",
                "TEXT_TOTAL_PRODUCTS_NET_PRICE": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div:nth-child(2) > span",
                "LABEL_TOTAL_SHIPPING_FEE": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div:nth-child(3) > strong",
                "TEXT_TOTAL_SHIPPING_FEE": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div:nth-child(3) > span",
                "LABEL_TOTAL_PRODUCTS_GROSS_PRICE": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div:nth-child(4) > strong",
                "TEXT_TOTAL_PRODUCTS_GROSS_PRICE": "css=#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div:nth-child(4) > span"
            }
        }
    }
}

HOMEPAGE = {
    "SLIDER": {
        "CONTAINER": "css=#homepage-slider > div",
        "PREV_ARROW": "css=#homepage-slider > div > div.bx-controls.bx-has-controls-direction > div > a.bx-prev",
        "NEXT_ARROW": "css=#homepage-slider > div > div.bx-controls.bx-has-controls-direction > div > a.bx-next",
        "IMAGE_SAMPLE_1": {
            "IMAGE": "css=#homeslider > li:nth-child(2) > a > img",
            "IMAGE_TITLE": "css=#homeslider > li:nth-child(2) > div > h2",
            "IMAGE_CAPTION": "css=#homeslider > li:nth-child(2) > div > p:nth-child(2)",
            "BUTTON_SHOP_NOW": "css=#homeslider > li:nth-child(2) > div > p:nth-child(3) > button"
        },
        "IMAGE_SAMPLE_2": {
            "IMAGE": "css=#homeslider > li:nth-child(3) > a > img",
            "IMAGE_TITLE": "css=#homeslider > li:nth-child(3) > div > h2",
            "IMAGE_CAPTION": "css=#homeslider > li:nth-child(3) > div > p:nth-child(2)",
            "BUTTON_SHOP_NOW": "css=#homeslider > li:nth-child(3) > div > p:nth-child(3) > button"
        },
        "IMAGE_SAMPLE_3": {
            "IMAGE": "css=#homeslider > li:nth-child(4) > a > img",
            "IMAGE_TITLE": "css=#homeslider > li:nth-child(4) > div > h2",
            "IMAGE_CAPTION": "css=#homeslider > li:nth-child(4) > div > p:nth-child(2)",
            "BUTTON_SHOP_NOW": "css=#homeslider > li:nth-child(4) > div > p:nth-child(3) > button"
        }
    }
}

CHECKOUT = {
    "PAGE_HEADING": "id=cart_title",
    "TOTAL_PRODUCTS_QUANTITY": "id=summary_products_quantity",
    "BREADCRUMB_STEP": {
        "CONTAINER": "id=order_step",
        "STEP_1": "xpath=//*[@id='order_step']/li[1]",
        "STEP_2": "xpath=//*[@id='order_step']/li[2]",
        "STEP_3": "xpath=//*[@id='order_step']/li[3]",
        "STEP_4": "xpath=//*[@id='order_step']/li[4]",
        "STEP_5": "xpath=//*[@id='order_step']/li[5]",
    },
    "CART_SUMMARY": {
        "HEADER": {
            "1ST_COLUMN_PRODUCT": "css=#cart_summary > thead > tr > th:nth-child(1)",
            "2ND_COLUMN_DESCRIPTION": "css=#cart_summary > thead > tr > th:nth-child(2)",
            "3RD_COLUMN_AVAILABILITY": "css=#cart_summary > thead > tr > th:nth-child(3)",
            "4TH_COLUMN_UNIT_PRICE": "css=#cart_summary > thead > tr > th:nth-child(4)",
            "5TH_COLUMN_QUANTITY": "css=#cart_summary > thead > tr > th:nth-child(5)",
            "6TH_COLUMN_TOTAL": "css=#cart_summary > thead > tr > th:nth-child(6)",
            "7TH_COLUMN_DELETE": "css=#cart_summary > thead > tr > th:nth-child(7)",
        },
        "BODY": {
            "ONE_BASE_INDEXED_ARRAY_PRODUCT_RECORD": "xpath=//table[@id='cart_summary']/tbody/tr",
            "PRODUCT_NAME": "/td[2]/p/a",
            "PRODUCT_UNIT_PRICE": "/td[4]/span/span",
            "PRODUCT_QUANTITY": "/td[5]/input[2]",
            "PRODUCT_TOTAL": "/td[6]/span",
            "NET_GRAND_TOTAL": "id=total_product",
            "SHIPPING_FEE": "id=total_shipping",
            "NET_GRAND_TOTAL_WITHOUT_TAX": "id=total_price_without_tax",
            "TAX": "id=total_tax",
            "GROSS_GRAND_TOTAL": "id=total_price"
        }
    }
}