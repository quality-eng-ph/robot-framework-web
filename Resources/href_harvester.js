function arrayContains(arr, attribute) {
    if(arr.indexOf(attribute)!= -1)
        return true;
    return false;
}

var allElements = document.getElementsByTagName("a");
var allHrefs = [];
for (var index = 0; index < allElements.length; index++) {
    var element = allElements[index];
    if (element.href && /^(?!mailto).+/.test(element.href) && !arrayContains(allHrefs, element.href)) { //if there's a value and does not contain that element (filtering unique in list)
    	allHrefs.push(element.href);
    }
}
return allHrefs;