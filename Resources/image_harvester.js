function arrayContains(arr, attribute) {
    if(arr.indexOf(attribute)!= -1)
        return true;
    return false;
}

var allElements = document.getElementsByTagName("img");
var allImgs = [];
for (var index = 0; index < allElements.length; index++) {
    var element = allElements[index];
    if (element.src && !arrayContains(allImgs, element.src)) { //if there's a value and does not contain that element (filtering unique in list)
    	allImgs.push(element.src);
    }
}
return allImgs;