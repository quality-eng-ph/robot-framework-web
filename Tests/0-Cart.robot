*** Settings ***
Force Tags      homepage
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Maximize Browser Window
...                 AND         Go To   ${SUT_URL}
...                 AND         Common.Initialize Global Variables
...                 AND         Create Expected Cart
Suite Teardown  End Test
Test Setup      Run Keywords    Common.Wait Until Page Finishes Loading
...                 AND         Click Element   ${COMMON_random_category}
...                 AND         Common.Wait Until Page Finishes Loading
Test Teardown   Run Keywords    Run Keyword If Test Failed      Capture Page Screenshot
...                 AND         Clear Expected Cart
...                 AND         Clear Actual Cart

#Expected Cart List Structure:
#[
#	{
#		"product_name": "lala",
#		"product_quantity": "1",
#		"product_unit_price": "200.00"
#  	},
#  	{
#		"product_name": "olol",
#		"product_quantity": "3",
#		"product_unit_price": "753.21"
#  	}
#]

*** Keywords ***
Add Expected Item Directly To Expected Cart
    [Arguments]     ${CART_expected_cart}
    ...             ${product_name}
    ...             ${product_quantity}
    ...             ${product_unit_price}
    ${item}=    Create Dictionary
    ...             product_name=${product_name}
    ...             product_quantity=${product_quantity}
    ...             product_unit_price=${product_unit_price}
    Append To List  ${CART_expected_cart}   ${item}
    Set Suite Variable  ${CART_expected_cart}   ${CART_expected_cart}

Add Expected Item To Expected Cart
    [Arguments]     ${CART_expected_cart}
    ...             ${product_name}
    ...             ${product_quantity}
    ...             ${product_unit_price}
    Set Test Variable   ${is_existing_item}     ${FALSE}
    :FOR    ${item}     IN      @{CART_expected_cart}
    \   ${is_existing_item}=    Run Keyword And Return Status   Should Be Equal As Strings  ${item['product_name']}     ${product_name}
    \   Log     ${is_existing_item}
    \   ${cur_index}=   Get Index From List     ${CART_expected_cart}   ${item}
    \   Exit For Loop If    ${is_existing_item}
    Run Keyword If  ${is_existing_item}     Add Quantity To Item In Expected Cart   ${CART_expected_cart}   ${cur_index}     ${product_quantity}
    ...     ELSE    Add Expected Item Directly To Expected Cart     ${CART_expected_cart}   ${product_name}     ${product_quantity}     ${product_unit_price}

Add Quantity To Item In Expected Cart
    [Arguments]     ${CART_expected_cart}   ${index}     ${quantity}
    ${new_product_quantity}=    Evaluate    ${CART_expected_cart[${index}]['product_quantity']} + ${quantity}
    Remove From Dictionary      ${CART_expected_cart[${index}]}     product_quantity
    Set To Dictionary   ${CART_expected_cart[${index}]}   product_quantity=${new_product_quantity}
    # Reorder based on last modified:
    ${to_be_appended_dictionary_in_list}=   Remove From List    ${CART_expected_cart}   ${index}
    Append To List  ${CART_expected_cart}   ${to_be_appended_dictionary_in_list}
    Set Suite Variable  ${CART_expected_cart}   ${CART_expected_cart}

Add Random Items To Actual Cart From Category Page
    # Get number of products displayed in current page:
    ${current_page_hits_count}=     Execute Javascript  return document.querySelector('${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['EVAL_PRODUCT_LIST_COUNTER']}').childElementCount
    # Generate a random number to be used in determining the number of items in my shopping list:
    ${random_number_of_items_to_be_added}=  Common.Pick Randomly From Integer Range     1   ${MAX_ITEMS_TO_BE_ADDED_IN_CART}
    # Creating comparison cart:
    ${CART_expected_cart}=  Create Expected Cart
    # Adding products to cart:
    :FOR    ${counter}  IN RANGE    ${random_number_of_items_to_be_added}
    #   Determining which product I wish to add in acutal cart:
    \   ${random_item_index}=   Common.Pick Randomly From Integer Range     1   ${current_page_hits_count}
    #   Remembering the name of the product to be added in cart:
    \   ${product_name}=    Get Text    ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${random_item_index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['TEXT_PRODUCT_NAME']}
    #   Remembering the unit price of the product to be added in cart:
    \   ${product_unit_price}=  Get Text    ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${random_item_index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['TEXT_PRODUCT_PRICE']}
    #   Listing the aforementioned product in my comparison cart for later comparison
    \   Add Expected Item To Expected Cart  ${CART_expected_cart}   ${product_name}     1   ${product_unit_price}
    #   Now adding the aforementioned product in actual cart:
    \   Mouse Over  ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${random_item_index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['TEXT_PRODUCT_NAME']}
    \   Click Element   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${random_item_index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['BUTTON_ADD_TO_CART']}
    #   Product successfully added to your shopping cart spiel is displayed
    \   Common.Wait Until Page Finishes Loading
    \   Sleep   0.75
    \   Common.Wait Until Cart Confirmation Modal Is Visible
    \   Element Should Be Visible   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['MODAL_CART_CONFIRMATION']['PRODUCT_ITEM']['SPIEL_SUCCESSFUL_ADD_TO_CART']}
    \   Element Text Should Be      ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['MODAL_CART_CONFIRMATION']['PRODUCT_ITEM']['SPIEL_SUCCESSFUL_ADD_TO_CART']}    Product successfully added to your shopping cart
    #   If the aforementioned product is the last item in shopping list
    \   ${is_last_in_shopping_list}=    Evaluate     ${counter}+1 == ${random_number_of_items_to_be_added}
    #   Then, proceed to checkout:
    \   Run Keyword If  ${${is_last_in_shopping_list}}  Click Element   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['MODAL_CART_CONFIRMATION']['BUTTON_PROCEED_TO_CHECKOUT']}
    \   ...     ELSE        Click Element   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['MODAL_CART_CONFIRMATION']['BUTTON_CONTINUE_SHOPPING']}
    \   Common.Wait Until Page Finishes Loading
    \   Sleep   0.75
    # Now in checkout:
    Location Should Be      ${LOCATION_CHECKOUT}
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['ONE_BASE_INDEXED_ARRAY_TRAIL']}[1]
    Element Should Be Visible   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['LINK_SPECIAL_1ST_TRAIL']}
    Element Text Should Be      ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['LINK_SPECIAL_1ST_TRAIL']}   Your shopping cart
    # Comparing actual cart item total quantity against expected cart:
    ${expected_cart_total_quantity}=    Get Expected Cart Total Number Of Items     ${CART_expected_cart}
    ${is_expected_cart_total_quantity_plural}=  Evaluate   ${expected_cart_total_quantity} > 1
    Run Keyword If  ${is_expected_cart_total_quantity_plural}   Element Text Should Be      ${CHECKOUT['TOTAL_PRODUCTS_QUANTITY']}      ${expected_cart_total_quantity} Products
    ...     ELSE    Element Text Should Be      ${CHECKOUT['TOTAL_PRODUCTS_QUANTITY']}      ${expected_cart_total_quantity} Product
    # Setting counter:
    ${table_record_index}=  Set Variable    ${1}
    # Now comparing items in actual cart against expected cart:
    :FOR    ${item}     IN      @{CART_expected_cart}
    \   ${expected_product_name}=           Get From Dictionary     ${item}     product_name
    \   ${expected_product_unit_price}=     Get From Dictionary     ${item}     product_unit_price
    \   ${expected_product_quantity}=       Get From Dictionary     ${item}     product_quantity
    \   Element Text Should Be  ${CHECKOUT['CART_SUMMARY']['BODY']['ONE_BASE_INDEXED_ARRAY_PRODUCT_RECORD']}[${table_record_index}]${CHECKOUT['CART_SUMMARY']['BODY']['PRODUCT_NAME']}          ${expected_product_name}
    \   Element Text Should Be  ${CHECKOUT['CART_SUMMARY']['BODY']['ONE_BASE_INDEXED_ARRAY_PRODUCT_RECORD']}[${table_record_index}]${CHECKOUT['CART_SUMMARY']['BODY']['PRODUCT_UNIT_PRICE']}    ${expected_product_unit_price}
    \   ${actual_product_quantity}=     Get Value   ${CHECKOUT['CART_SUMMARY']['BODY']['ONE_BASE_INDEXED_ARRAY_PRODUCT_RECORD']}[${table_record_index}]${CHECKOUT['CART_SUMMARY']['BODY']['PRODUCT_QUANTITY']}
    \   Should Be Equal As Strings      ${actual_product_quantity}      ${expected_product_quantity}
    \   ${operand_expected_product_unit_price}=     Remove String       ${expected_product_unit_price}          $
    \   ${operand_expected_product_unit_price}=     Convert To Number   ${operand_expected_product_unit_price}  2
    \   ${operand_expected_product_quantity}=       Convert To Number   ${expected_product_quantity}            2
    \   ${expected_subtotal}=   Evaluate    ${operand_expected_product_unit_price} * ${operand_expected_product_quantity}
    \   ${expected_subtotal}=   Evaluate    "%0.2f" % ${expected_subtotal}
    \   ${expected_subtotal}=   Convert To String   ${expected_subtotal}
    \   ${expected_subtotal}=   Catenate    SEPARATOR=      $   ${expected_subtotal}
    \   Element Text Should Be  ${CHECKOUT['CART_SUMMARY']['BODY']['ONE_BASE_INDEXED_ARRAY_PRODUCT_RECORD']}[${table_record_index}]${CHECKOUT['CART_SUMMARY']['BODY']['PRODUCT_TOTAL']}     ${expected_subtotal}
    #   Update counter:
    \   ${table_record_index}=  Evaluate    ${table_record_index}+1
    # Checking if net grand total price (total products is correct):
    ${expected_net_grand_total_number}=    Get Expected Cart Net Grand Total   ${CART_expected_cart}
    ${expected_net_grand_total}=    Convert To String   ${expected_net_grand_total_number}
    ${expected_net_grand_total}=    Catenate    SEPARATOR=      $   ${expected_net_grand_total}
    Element Text Should Be      ${CHECKOUT['CART_SUMMARY']['BODY']['NET_GRAND_TOTAL']}      ${expected_net_grand_total}
    ${expected_shipping_fee_number}=    Evaluate    "%0.2f" % ${SHIPPING_FEE}
    ${expected_shipping_fee}=    Convert To String   ${expected_shipping_fee_number}
    ${expected_shipping_fee}=    Catenate    SEPARATOR=      $   ${expected_shipping_fee}
    Element Text Should Be      ${CHECKOUT['CART_SUMMARY']['BODY']['SHIPPING_FEE']}      ${expected_shipping_fee}
    ${expected_net_grand_total_without_tax_number}=     Evaluate    "%0.2f" % (${expected_net_grand_total_number} + ${expected_shipping_fee_number})
    ${expected_net_grand_total_without_tax}=    Convert To String   ${expected_net_grand_total_without_tax_number}
    ${expected_net_grand_total_without_tax}=    Catenate    SEPARATOR=      $   ${expected_net_grand_total_without_tax}
    Element Text Should Be      ${CHECKOUT['CART_SUMMARY']['BODY']['NET_GRAND_TOTAL_WITHOUT_TAX']}      ${expected_net_grand_total_without_tax}
    ${tax}=     Get Text    ${CHECKOUT['CART_SUMMARY']['BODY']['TAX']}
    ${tax}=         Remove String   ${tax}  $
    ${tax_number}=  Convert To Number   ${tax}  2
    ${tax_number}=  Evaluate    "%0.2f" % ${tax_number}
    ${expected_gross_grand_total_number}=   Evaluate    "%0.2f" % (${expected_net_grand_total_without_tax_number} + ${tax_number})
    ${expected_gross_grand_total}=  Convert To String   ${expected_gross_grand_total_number}
    ${expected_gross_grand_total}=  Catenate    SEPARATOR=      $   ${expected_gross_grand_total}
    Element Text Should Be  ${CHECKOUT['CART_SUMMARY']['BODY']['GROSS_GRAND_TOTAL']}    ${expected_gross_grand_total}

Expected Cart Should Be Empty
    [Arguments]     ${CART_expected_cart}
    Should Be Empty     ${CART_expected_cart}

Expected Cart Should Not Be Empty
    [Arguments]     ${CART_expected_cart}
    Should Not Be Empty     ${CART_expected_cart}

Clear Actual Cart
    [Documentation]     Empties the cart by clearing the session cookies.
    Delete All Cookies

Clear Expected Cart
    [Documentation]     Empties the cart by clearing the session cookies.
    Set Suite Variable  ${CART_expected_cart}   ${EMPTY}
    Expected Cart Should Be Empty   ${CART_expected_cart}

Create Expected Cart
    ${CART_expected_cart}=      Create List
    Set Suite Variable  ${CART_expected_cart}   ${CART_expected_cart}
    [Return]    ${CART_expected_cart}

Get Expected Cart Net Grand Total
    [Arguments]     ${CART_expected_cart}
    ${net_grand_total}=   Set Variable    ${0.00}
    :FOR    ${element}  IN  @{CART_expected_cart}
    \   ${operand_expected_product_quantity}=   Get From Dictionary     ${element}      product_quantity
    \   ${operand_expected_product_quantity}=   Convert To Number       ${operand_expected_product_quantity}    2
    \   ${operand_expected_product_unit_price}=     Get From Dictionary     ${element}      product_unit_price
    \   ${operand_expected_product_unit_price}=     Remove String       ${operand_expected_product_unit_price}  $
    \   ${operand_expected_product_unit_price}=     Convert To Number   ${operand_expected_product_unit_price}  2
    \   ${expected_subtotal}=   Evaluate    ${operand_expected_product_unit_price} * ${operand_expected_product_quantity}
    \   ${net_grand_total}=   Evaluate    ${net_grand_total} + ${expected_subtotal}
    ${net_grand_total}=   Evaluate    "%0.2f" % ${net_grand_total}
    [Return]        ${net_grand_total}

Get Expected Cart Total Number Of Items
    [Arguments]     ${CART_expected_cart}
    ${total}=   Set Variable    ${0}
    :FOR    ${element}  IN  @{CART_expected_cart}
    \   ${product_quantity}=    Get From Dictionary     ${element}      product_quantity
    \   ${product_quantity}=    Convert To Integer  ${product_quantity}
    \   ${total}=   Evaluate    ${total} + ${product_quantity}
    [Return]        ${total}

Get Expected Cart Total Number Of Unique Items
    [Arguments]     ${CART_expected_cart}
    ${count_total_unique_items}=  Get Length    ${CART_expected_cart}
    [Return]        ${count_total_unique_items}

*** Test Cases ***
Header Page Section Test
    [Tags]      category    smoke   visuals
    Common.Header Page Section Test

Footer Page Section Test
    [Tags]      category    smoke   visuals
    Common.Footer Page Section Test

#Test Cart Functions
#    [Tags]      this
#    ${CART_expected_cart}=  Create Expected Cart
#    Add Expected Item To Expected Cart  ${CART_expected_cart}    Printed Dress   2   200.00
#    Add Expected Item To Expected Cart  ${CART_expected_cart}    Blouse  3   17.65
#    Add Expected Item To Expected Cart  ${CART_expected_cart}    Printed Dress   1   200.00
#    Log     ${CART_expected_cart}

User should see added items are in cart
    [Tags]      cart    smoke   functional
    Add Random Items To Actual Cart From Category Page

Click Login
    Open Browser	http://automationpractice.com	firefox
    Click Element	xpath=.//*[@id='header']/div[2]/div/div/nav/div[1]/a


