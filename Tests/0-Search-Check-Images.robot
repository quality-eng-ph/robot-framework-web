*** Settings ***
Default Tags    search  detailed_smoke  health_check
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Go To   ${SUT_URL}
Suite Teardown  End Test
Test Setup      Common.Wait Until Page Finishes Loading

*** Variables ***
# Forcing browser to be phantomjs by overriding ${BROWSER} variable from Config.
${BROWSER}      phantomjs

*** Test Cases ***
User should be able to see the favicon in search page
    [Tags]      search  favicon     smoke   visuals
    ${favicon_URL}=     Common.Get Page Favicon URL
    Common.Non-critical Health Check    ${favicon_URL}

User should not see broken image(s) in search page
    [Setup]     Run Keywords    Input Text  ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   ${COMMON_search_keyword}
    ...             AND         Press Key   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   \\13
    ...             AND         Common.Wait Until Page Finishes Loading
    Common.Check For Broken Image Source