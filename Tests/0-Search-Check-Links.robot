*** Settings ***
Default Tags    search  detailed_smoke  health_check
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Go To   ${SUT_URL}
Suite Teardown  End Test
Test Setup      Common.Wait Until Page Finishes Loading

*** Variables ***
# Forcing browser to be phantomjs by overriding ${BROWSER} variable from Config.
${BROWSER}      phantomjs

*** Test Cases ***
User should not encounter broken link(s) in search page
    [Setup]     Run Keywords    Input Text  ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   ${COMMON_search_keyword}
    ...             AND         Press Key   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   \\13
    ...             AND         Common.Wait Until Page Finishes Loading
    Common.Check For Broken Hyperlinks