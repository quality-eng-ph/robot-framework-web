*** Settings ***
Force Tags      homepage
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Maximize Browser Window
...                 AND         Go To   ${SUT_URL}
...                 AND         Common.Initialize Global Variables
Suite Teardown  End Test
Test Setup      Common.Wait Until Page Finishes Loading

*** Keywords ***
Get regular expression pattern of the search query
    [Documentation]     Returns the regex string of the given search keyword by replacing all occurences of space with ``.*``.
    [Arguments]         ${keyword}
    ${regex}=   Replace String  string=${keyword}   search_for=${SPACE}     replace_with=.*     count=-1
    ${regex}=   Catenate    SEPARATOR=  .*  ${regex}    .*
    [Return]    ${regex}

Get list of hits in current search page
    [Documentation]     Returns a list of string values of the search list.
    Location should contain     ${LOCATION_SEARCH}
    Mouse Out   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}
    Common.Wait Until Page Finishes Loading
    ${current_page_hits_count}=     Execute Javascript  return document.querySelector('${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['EVAL_PRODUCT_LIST_COUNTER']}').childElementCount
    ${list_of_product_names}=   Create List
    :FOR    ${index}    IN RANGE    1   ${current_page_hits_count}+1
    \   ${hit_text}=    Get Text    ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['TEXT_PRODUCT_NAME']}
    \   Append To List  ${list_of_product_names}    ${hit_text}
    Log     ${list_of_product_names}
    [Return]    ${list_of_product_names}

Search a keyword in search bar by clicking search button
    [Arguments]     ${keyword}
    Clear Element Text      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}
    Input Text      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   ${keyword}
    Press Key       ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   \\13
    Common.Wait Until Page Finishes Loading

Search a keyword in search bar by pressing enter key
    [Arguments]     ${keyword}
    Clear Element Text      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}
    Input Text      ${UNIVERSAL_OBJECTS['HEADER_SECTION']['TEXTBOX_SEARCH_BAR']}   ${keyword}
    Click Element   ${UNIVERSAL_OBJECTS['HEADER_SECTION']['BUTTON_SEARCH']}
    Common.Wait Until Page Finishes Loading

Search results should match search keyword
    [Documentation]     Fails if the search results does not match with the regex pattern of the search keyword.
    [Arguments]     ${keyword}
    ${search_keyword_regex}=    Get regular expression pattern of the search query  ${keyword}
    ${list_of_product_names}=   Get list of hits in current search page
    :FOR    ${hit}  IN  @{list_of_product_names}
    \   Run Keyword And Continue On Failure     Should Match Regexp     ${hit}     ${search_keyword_regex}

*** Test Cases ***
Header Page Section Test
    [Tags]      search  smoke   visuals
    [Setup]     Search a keyword in search bar by pressing enter key    ${COMMON_search_keyword}
    Common.Header Page Section Test

Footer Page Section Test
    [Tags]      search  smoke   visuals
    [Setup]     Search a keyword in search bar by pressing enter key    ${COMMON_search_keyword}
    Common.Footer Page Section Test

User should be able to see the correct page title of the search page
    [Tags]      search  smoke   functional
    [Setup]     Search a keyword in search bar by pressing enter key    ${COMMON_search_keyword}
    Title Should Be     Search - My Store

User should be able to see the correct breadcrumb trail of the search page
    [Tags]      search  smoke   visuals
    [Setup]     Search a keyword in search bar by pressing enter key    ${COMMON_search_keyword}
    Run Keyword And Continue On Failure     Element Should Be Visible   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['SECTION']}
    Run Keyword And Continue On Failure     Element Should Be Visible   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['ONE_BASE_INDEXED_ARRAY_TRAIL']}[1]
    Run Keyword And Continue On Failure     Element Should Be Visible   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['LINK_SPECIAL_1ST_TRAIL']}
    Run Keyword And Continue On Failure     Selenium2Library.Element Text Should Be      ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['LINK_SPECIAL_1ST_TRAIL']}  Search

User should be able to go to homepage upon clicking the home icon from the breadcrumb
    [Tags]      search  smoke   visuals     homepage
    [Setup]     Search a keyword in search bar by pressing enter key    ${COMMON_search_keyword}
    [Teardown]  Go Back
    Click Element   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['BREADCRUMB']['ONE_BASE_INDEXED_ARRAY_TRAIL']}[1]
    Location Should Be  ${LOCATION_HOME_PAGE}

User should be able to see the search results upon clicking search button from the search bar
    [Tags]      search  smoke   visuals
    Search a keyword in search bar by clicking search button    ${COMMON_search_keyword}
    Element Should Be Visible   css=${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['EVAL_PRODUCT_LIST_COUNTER']}

User should be able to see the search results upon hitting enter key from the search bar
    [Tags]      search  smoke   visuals
    Search a keyword in search bar by pressing enter key        ${COMMON_search_keyword}
    Element Should Be Visible   css=${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['EVAL_PRODUCT_LIST_COUNTER']}

User should be able to see related hits result from the search keyword
    [Tags]      search  smoke   functional
    [Setup]     Search a keyword in search bar by pressing enter key        ${COMMON_search_keyword}
    Search results should match search keyword  ${COMMON_search_keyword}

User should be able to see price tags on all products
    [Tags]      search  smoke   visuals
    [Setup]     Search a keyword in search bar by pressing enter key        ${COMMON_search_keyword}
    ${current_page_hits_count}=     Execute Javascript  return document.querySelector('${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['EVAL_PRODUCT_LIST_COUNTER']}').childElementCount
    :FOR    ${index}    IN RANGE    1   ${current_page_hits_count}+1
    \   ${hit_price}=    Get Text   ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['TEXT_PRODUCT_PRICE']}
    \   Run Keyword And Continue On Failure     Should Not Be Equal As Strings      ${hit_price}    ${EMPTY}

User should be able to see in stock label on all products
    [Tags]      search  smoke   visuals
    [Setup]     Search a keyword in search bar by pressing enter key        ${COMMON_search_keyword}
    ${current_page_hits_count}=     Execute Javascript  return document.querySelector('${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['EVAL_PRODUCT_LIST_COUNTER']}').childElementCount
    :FOR    ${index}    IN RANGE    1   ${current_page_hits_count}+1
    \   ${hit_availability}=    Get Text    ${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['ONE_BASE_INDEXED_ARRAY_PRODUCT_LIST']}[${index}]${UNIVERSAL_OBJECTS['MAIN_BODY_SECTION']['HITS']['TEXT_PRODUCT_AVAILABILITY']}
    \   Run Keyword And Continue On Failure     Should Be Equal As Strings      ${hit_availability}     In stock