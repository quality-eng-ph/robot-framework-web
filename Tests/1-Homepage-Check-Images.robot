*** Settings ***
Default Tags    homepage    detailed_smoke  health_check
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Go To   ${SUT_URL}
Suite Teardown  End Test
Test Setup      Common.Wait Until Page Finishes Loading

*** Variables ***
# Forcing browser to be phantomjs by overriding ${BROWSER} variable from Config.
${BROWSER}      phantomjs

*** Test Cases ***
User should be able to see the favicon in homepage
    [Tags]      homepage    favicon     smoke   visuals
    ${favicon_URL}=     Common.Get Page Favicon URL
    Common.Non-critical Health Check    ${favicon_URL}

User should not see broken image(s) in homepage
    Common.Check For Broken Image Source