*** Settings ***
Default Tags    homepage    detailed_smoke  health_check
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Go To   ${SUT_URL}
Suite Teardown  End Test
Test Setup      Common.Wait Until Page Finishes Loading

*** Variables ***
# Forcing browser to be phantomjs by overriding ${BROWSER} variable from Config.
${BROWSER}      phantomjs

*** Test Cases ***
User should not encounter broken link(s) in homepage
    Common.Check For Broken Hyperlinks