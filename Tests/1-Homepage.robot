*** Settings ***
Force Tags      homepage
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Run Keywords    Begin Test
...                 AND         Maximize Browser Window
...                 AND         Go To   ${SUT_URL}
Suite Teardown  End Test
Test Setup      Common.Wait Until Page Finishes Loading

*** Keywords ***
Is Slider Image Visible
    [Documentation]     Returns ``True`` if the current coordinates of the image in the slider is not the same as
    ...                 the current coordinates of the container of the slider; otherwise, returns ``False``.
    [Arguments]         ${image_locator}
    ${container_x_coordinate}=      Get Horizontal Position     ${HOMEPAGE['SLIDER']['CONTAINER']}
    ${container_y_coordinate}=      Get Vertical Position     ${HOMEPAGE['SLIDER']['CONTAINER']}
    ${image_x_coordinate}=          Get Horizontal Position     ${image_locator}
    ${image_y_coordinate}=          Get Vertical Position     ${image_locator}
    ${comparison_results}=          Evaluate    ${container_x_coordinate} == ${image_x_coordinate} and ${container_y_coordinate} == ${image_y_coordinate}
    [Return]            ${comparison_results}

*** Test Cases ***
Header Page Section Test
    [Tags]      homepage    header_section  smoke   visuals
    Common.Header Page Section Test

Footer Page Section Test
    [Tags]      homepage    footer_section  smoke   visuals
    Common.Footer Page Section Test

User should be able to see the correct page title of the Homepage
    [Tags]      homepage    smoke   functional
    Title Should Be     My Store

User should be able to view the Contact us page when Contact us button is clicked
    [Tags]      homepage    header_section  contact_us  smoke   functional
    [Teardown]  Go To       ${SUT_URL}
    Keywords-Universal-Header.Click Contact Us Button
    Location Should Be  ${LOCATION_CONTACT_US}
    Title Should Be     Contact us - My Store

User should be able to view the Sign in page when Sign in button is clicked
    [Tags]      homepage    header_section  sign_in     smoke   functional
    [Teardown]  Go To       ${SUT_URL}
    Keywords-Universal-Header.Click Sign In Button
    Location Should Contain    ${LOCATION_SIGN_IN}
    Title Should Be     Login - My Store

User should be able to view the Homepage when the logo is clicked
    [Tags]      homepage    header_section  smoke   functional
    Keywords-Universal-Header.Click Header Logo
    Location Should Be  ${LOCATION_HOME_PAGE}
    Title Should Be     My Store

User should see images in slider are auto-rotating
    [Tags]      homepage    slider  smoke   visuals
    [Documentation]     Image in the slider should not be the same image after three seconds.
    # Getting the currently displayed image on 1st inspection
    :FOR    ${tmp}  IN RANGE   10
    \   ${is_image_1_visible_1st}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_1']['IMAGE']}
    \   ${is_image_2_visible_1st}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_2']['IMAGE']}
    \   ${is_image_3_visible_1st}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_3']['IMAGE']}
    \   ${sum}=     Evaluate    ${is_image_1_visible_1st} + ${is_image_2_visible_1st} + ${is_image_3_visible_1st}
    \   Exit For Loop If    ${sum} == ${1}
    # Sleeping for 3 seconds for the image in the slider to auto-rotate
    Sleep   3
    # Getting the currently displayed image on 2nd inspection
    :FOR    ${tmp}  IN RANGE   10
    \   ${is_image_1_visible_2nd}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_1']['IMAGE']}
    \   ${is_image_2_visible_2nd}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_2']['IMAGE']}
    \   ${is_image_3_visible_2nd}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_3']['IMAGE']}
    \   ${sum}=     Evaluate    ${is_image_1_visible_2nd} + ${is_image_2_visible_2nd} + ${is_image_3_visible_2nd}
    \   Exit For Loop If    ${sum} == ${1}
    # A sum of 2 or higher would mean the image was displayed twice or more than twice.
    # Hence, asserting that the image should only be displayed once or never in the period of three seconds.
    ${check_if_image_1_rotates}=    Evaluate    ( ${is_image_1_visible_1st} + ${is_image_1_visible_2nd} ) <= 1
    ${check_if_image_2_rotates}=    Evaluate    ( ${is_image_2_visible_1st} + ${is_image_2_visible_2nd} ) <= 1
    ${check_if_image_3_rotates}=    Evaluate    ( ${is_image_3_visible_1st} + ${is_image_3_visible_2nd} ) <= 1
    Should Be True  ${check_if_image_1_rotates}
    Should Be True  ${check_if_image_2_rotates}
    Should Be True  ${check_if_image_3_rotates}

User should only see one image in slider
    [Tags]      homepage    slider  smoke   visuals
    [Documentation]     Fails if there is more than one image in the slider visible in an instance.
    ...                 Clicking the previous or next arrow will stop the slider rotation.
    ...                 Slider rotation should be stopped in order to accurately assert that only one image is visible at a given time.
    [Teardown]  Run Keywords    Run Keyword If Test Failed  Capture Page Screenshot
    ...             AND         Reload Page
    ${browse_slider_by_n_times}=    Common.Pick Randomly From Integer Range     1   3
    :FOR    ${ctr}  IN RANGE    ${browse_slider_by_n_times}
    \   ${arrow_locator}=   Common.Pick Randomly From Arguments     ${HOMEPAGE['SLIDER']['PREV_ARROW']}     ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    \   Click Element   ${arrow_locator}
    \   Mouse Out       ${arrow_locator}
    \   Sleep   1.5
    ${is_image_1_visible}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_1']['IMAGE']}
    ${is_image_2_visible}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_2']['IMAGE']}
    ${is_image_3_visible}=  Is Slider Image Visible     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_3']['IMAGE']}
    ${sum}=     Evaluate    ${is_image_1_visible} + ${is_image_2_visible} + ${is_image_3_visible}
    Should Be Equal As Integers     ${sum}  ${1}

User should see complete caption of the image in slider
    [Tags]      homepage    slider  smoke   visuals
    [Teardown]  Run Keywords    Run Keyword If Test Failed  Capture Page Screenshot
    ...             AND         Reload Page
    ${locator}=     Common.Pick Randomly From Arguments     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_1']}     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_2']}     ${HOMEPAGE['SLIDER']['IMAGE_SAMPLE_3']}
    :FOR    ${tmp}  IN RANGE    3
    \   ${is_certain_image_visible}=    Is Slider Image Visible     ${locator['IMAGE']}
    \   Exit For Loop If    ${is_certain_image_visible}
    \   Sleep   1.5
    Run Keyword And Continue On Failure     Selenium2Library.Element Text Should Be      ${locator['IMAGE_TITLE']}       EXCEPTEUR\nOCCAECAT
    Run Keyword And Continue On Failure     Selenium2Library.Element Text Should Be      ${locator['IMAGE_CAPTION']}     Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit
    Run Keyword And Continue On Failure     Selenium2Library.Element Text Should Be      ${locator['BUTTON_SHOP_NOW']}   SHOP NOW !
    Run Keyword And Continue On Failure     Mouse Over  ${locator['BUTTON_SHOP_NOW']}
    ${button_background_color}=     Run Keyword And Continue On Failure     Common.Get Element Background Color     ${locator['BUTTON_SHOP_NOW']}
    Should Be Equal As Strings      ${button_background_color}  rgb(235, 235, 235)
    Run Keyword And Continue On Failure     Mouse Out   ${locator['BUTTON_SHOP_NOW']}
    ${button_background_color}=     Run Keyword And Continue On Failure     Common.Get Element Background Color     ${locator['BUTTON_SHOP_NOW']}
    Should Be Equal As Strings      ${button_background_color}  rgb(255, 255, 255)

User should be redirected to prestashop website with slider referral
    [Tags]      homepage    slider  smoke   functional
    [Teardown]  Run Keywords    Run Keyword If Test Failed  Capture Page Screenshot
    ...             AND         Go To   ${SUT_URL}
    ${browse_slider_by_n_times}=    Common.Pick Randomly From Integer Range     1   3
    :FOR    ${ctr}  IN RANGE    ${browse_slider_by_n_times}
    \   ${arrow_locator}=   Common.Pick Randomly From Arguments     ${HOMEPAGE['SLIDER']['PREV_ARROW']}     ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    \   Click Element   ${arrow_locator}
    \   Mouse Out       ${arrow_locator}
    \   Sleep   1.5
    Click Element       ${HOMEPAGE['SLIDER']['CONTAINER']}
    Location Should Be  ${LOCATION_PRETASHOP_REFERRAL}

User should see non-highlighted previous arrow of slider on non-hover
    [Tags]      homepage    slider  smoke   visuals
    [Setup]     Mouse Out   ${HOMEPAGE['SLIDER']['PREV_ARROW']}
    ${color}=   Get Element Text Color  ${HOMEPAGE['SLIDER']['PREV_ARROW']}
    Should Be Equal As Strings  ${color}    rgb(119, 119, 119)

User should see highlighted previous arrow of slider on hover
    [Tags]      homepage    slider  smoke   visuals
    [Setup]     Mouse Out   ${HOMEPAGE['SLIDER']['PREV_ARROW']}
    Mouse Over  ${HOMEPAGE['SLIDER']['PREV_ARROW']}
    ${color}=   Get Element Text Color  ${HOMEPAGE['SLIDER']['PREV_ARROW']}
    Should Be Equal As Strings  ${color}    rgb(81, 81, 81)

User should see non-highlighted next arrow of slider on non-hover
    [Tags]      homepage    slider  smoke   visuals
    [Setup]     Mouse Out   ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    ${color}=   Get Element Text Color  ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    Should Be Equal As Strings  ${color}    rgb(119, 119, 119)

User should see highlighted next arrow of slider on hover
    [Tags]      homepage    slider  smoke   visuals
    [Setup]     Mouse Out   ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    Mouse Over  ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    ${color}=   Get Element Text Color  ${HOMEPAGE['SLIDER']['NEXT_ARROW']}
    Should Be Equal As Strings  ${color}    rgb(81, 81, 81)